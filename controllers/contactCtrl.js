angular.module('myApp')
    .controller('ContactCtrl', ['ContactsService', '$scope',
        function(ContactsService, $scope) {
            $scope.contacts = ContactsService.contacts;
            $scope.allContacts = $scope.contacts;
            $scope.arrangeBy = 'firstname';
            $scope.group = 'all';
            $scope.test = true;
            $scope.selectedContacts = 0;

            $scope.setArrange = function(aNameField) {
                $scope.arrangeBy = aNameField;
            }

            $scope.toggleSelected = function(aContact) {
                var contact = _.find($scope.allContacts, function(contact) {
                    return contact == aContact;
                });

                if (contact.selected === 'true') {
                    contact.selected = 'false';
                    $scope.selectedContacts--;
                } else {
                    contact.selected = 'true';
                    $scope.selectedContacts++;
                }
            }

            $scope.setGroup = function(aGroup) {
                $scope.group = aGroup;

                if ($scope.arrangeBy === 'firstname') {
                    $scope.allContacts.sort(function(a,b) {
                        var alc = a.firstname.toLowerCase(), blc =
                            b.firstname.toLowerCase();
                        return alc > blc ? 1 : alc < blc ? -1 : 0;
                    });
                } else {
                    $scope.allContacts.sort(function(a,b) {
                        var alc = a.lastname.toLowerCase(), blc =
                            b.lastname.toLowerCase();
                        return alc > blc ? 1 : alc < blc ? -1 : 0;
                    });
                }

                var letters = [];
                switch (aGroup) {
                    case 'all':
                        $scope.contacts = $scope.allContacts;
                        return;
                        break;
                    case 'a-e':
                        letters = ['a', 'b', 'c', 'd', 'e'];
                        break;
                    case 'f-k':
                        letters = ['f', 'g', 'h', 'i', 'j', 'k'];
                        break;
                    case 'l-p':
                        letters = ['l', 'm', 'n', 'o', 'p'];
                        break;
                    case 'q-v':
                        letters = ['q', 'r', 's', 't', 'u', 'v'];
                        break;
                    case 'w-z':
                        letters = ['w', 'x', 'y', 'z'];
                        break;
                }
                $scope.contacts = [];
                for (var i = 0; i < $scope.allContacts.length; i++) {
                    for (var j = 0; j < letters.length; j++) {
                        var name;
                        if ($scope.arrangeBy === 'firstname') {
                            name = $scope.allContacts[i].firstname;
                        } else {
                            name = $scope.allContacts[i].lastname;
                        }
                        var letter = letters[j];
                        if (name[0] === letter.toUpperCase()) {
                            $scope.contacts.push($scope.allContacts[i]);
                        }
                    }
                }
            }
        }]);
