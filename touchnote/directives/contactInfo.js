angular.module('myApp')
    .directive('contactInfo', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'partials/contact.html'
        };
    });
