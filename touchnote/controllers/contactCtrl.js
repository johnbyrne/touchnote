angular.module('myApp')
    .controller('ContactCtrl', ['ContactsService', '$scope',
        function(ContactsService, $scope) {
            $scope.contacts = ContactsService.contacts;
            $scope.allContacts = $scope.contacts;
            $scope.arrangeBy = 'firstname';
            $scope.group = 'all';
            $scope.test = true;

            $scope.setArrange = function(aNameField) {
                $scope.arrangeBy = aNameField;
            }

            $scope.setGroup = function(aGroup) {
                $scope.group = aGroup;
                var letters = [];
                switch (aGroup) {
                    case 'all':
                        $scope.contacts = $scope.allContacts;
                        $scope.sortContacts();
                        return;
                        break;
                    case 'a-e':
                        letters = ['a', 'b', 'c', 'd', 'e'];
                        break;
                    case 'f-k':
                        letters = ['f', 'g', 'h', 'i', 'j', 'k'];
                        break;
                    case 'l-p':
                        letters = ['l', 'm', 'n', 'o', 'p'];
                        break;
                    case 'q-v':
                        letters = ['q', 'r', 's', 't', 'u', 'v'];
                        break;
                    case 'w-z':
                        letters = ['w', 'x', 'y', 'z'];
                        break;
                }
                $scope.contacts = [];
                for (var i = 0; i < $scope.allContacts.length; i++) {
                    for (var j = 0; j < letters.length; j++) {
                        var name;
                        if ($scope.arrangeBy === 'firstname') {
                            name = $scope.allContacts[i].firstname;
                        } else {
                            name = $scope.allContacts[i].lastname;
                        }
                        var letter = letters[j];
                        if (name[0] === letter.toUpperCase()) {
                            $scope.contacts.push($scope.allContacts[i]);
                        }
                    }
                }
            }
        }]);
