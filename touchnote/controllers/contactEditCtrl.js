angular.module('myApp')
    .controller('ContactEditCtrl', ['$scope', '$routeParams', '$location',
        function($scope, $routeParams, $location) {
    var newContact = false;
    if ($routeParams.contactId) {
        $scope.contact = $scope.contacts[$routeParams.contactId];
    } else {
        $scope.contact = {};
        newContact = true;
    }
    $scope.saveContact = function() {
        if (newContact) {
            $scope.contacts.push($scope.contact);
        }
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem($scope.contact.name, JSON.stringify({
                "address":$scope.contact.address,
                "linetwo":$scope.contact.linetwo,
                "town":$scope.contact.town,
                "county":$scope.contact.county,
                "postalcode":$scope.contact.postalcode
            }));
        }
        $scope.setGroup($scope.group);
        $location.path("/list");
    };
}]);