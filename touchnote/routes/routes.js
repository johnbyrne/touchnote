var app = angular.module('myApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/edit/:contactId', {
            templateUrl: 'templates/editContact.html',
            controller: 'ContactEditCtrl'
        });
        $routeProvider.when('/list', {
            templateUrl: 'templates/contactList.html',
            controller: 'ContactListCtrl'
        });
        $routeProvider.when('/edit', {
            templateUrl: 'templates/editContact.html',
            controller: 'ContactEditCtrl'
        });
        $routeProvider.otherwise({ redirectTo: '/list' });
    }]);
