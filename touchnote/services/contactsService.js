angular.module('myApp')
    .service('ContactsService', ['$rootScope', function($rootScope) {

        return {
            contacts : [{
                "firstname":"Adam",
                "lastname":"Williams",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Brian",
                "lastname":"Jones",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Charles",
                "lastname":"Doe",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"David",
                "lastname":"Walker",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Edward",
                "lastname":"McDonald",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Fredrich",
                "lastname":"Muller",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"George",
                "lastname":"Rodriguez",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Harry",
                "lastname":"Simpson",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Ian",
                "lastname":"Bailey",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"John",
                "lastname":"Bradford",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Liam",
                "lastname":"Carter",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Mike",
                "lastname":"Maxwell",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Nick",
                "lastname":"Payne",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Orlando",
                "lastname":"Jones",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Peter",
                "lastname":"Smith",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Raphael",
                "lastname":"White",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Simon",
                "lastname":"Royston",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Tom",
                "lastname":"Taylor",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Victor",
                "lastname":"Thompson",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"William",
                "lastname":"Smith",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            },{
                "firstname":"Yani",
                "lastname":"Thompson",
                "address":"123 The Street",
                "linetwo":"",
                "town":"",
                "county":"London",
                "postalcode":"N1 2SW"
            }]
        };
    }]);